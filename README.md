# kafka-retry

Library extending capabilities of retry to kafka consumers and producers

---

V0.1.0
------

* KafkaRetryConsumer added
* Configuration support for consumer